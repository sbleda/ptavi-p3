#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import smallsmilhandler
import json
import urllib.request


class KaraokeLocal():

    def __init__(self):
        parser = make_parser()
        cHandler = smallsmilhandler.SmallSMILHandler()
        parser.setContentHandler(cHandler)
        self.variable = False
        self.vari = False
        self.v = []

        try:
            parser.parse(open(fichero))

        except IndexError:
            sys.exit("Usage:python3 karaoke.py file.smil.")
        self.lista = cHandler.get_tags()

    def __str__(self):
        n = -1
        for elementos in self.lista:
            n = n+1
            for clave, valor in self.lista[n].items():
                if self.variable is False:
                    print(clave + '=' + valor, '\t', end="")

                if self.variable is True:
                    if clave == 'src':
                        val = valor.split('://')

                        if val[0] == 'http':
                            lugar = val[1].split('/')

                            valor = lugar[-1]
                    self.v.append(clave+'='+valor)
                    self.vari = True
                    print(clave + '=' + valor, '\t', end="")
            print()

    def to_json(self, fichero):
        if self.vari is False:
            fich = fichero.split('.')
            fi = fich[0] + '.json'
            with open(fi, 'w') as f:
                json.dump(self.lista, f, indent=2)
        if self.vari is True:
            with open('local.json', 'w') as f:
                json.dump(self.v, f, indent=3)

    def do_local(self):
        self.variable = True
        n = -1
        for elementos in self.lista:
            n = n+1
            for clave, valor in self.lista[n].items():
                if clave == 'src':
                    val = valor.split('://')

                    if val[0] == 'http':
                        lugar = val[1].split('/')
                        urllib.request.urlretrieve(valor, lugar[-1])

if __name__ == "__main__":
    """
    Programa principal
    """

    try:
        fichero = (sys.argv[1])

    except IndexError:
        sys.exit("Usage:python3 karaoke.py file.smil.")

    karaoke = KaraokeLocal()  # va a usar las cosas de la clase KaraokeLocal
    karaoke.__str__()
    karaoke.to_json(fichero)
    karaoke.do_local()
    karaoke.__str__()
    karaoke.to_json(fichero)
